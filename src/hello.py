from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello from Docker!'

def generate_response(variable):
    env_variable = os.environ.get(variable)
    if env_variable:
        return(env_variable)
    else:
        return("ERROR: {0} is not set".format(variable)), 404
    

@app.route('/version')
def version():
    return generate_response("VERSION")

@app.route('/devops')
def devops():
    return generate_response("EMAIL")

@app.route('/hostname')
def hostname():
    return generate_response("HOSTNAME")

@app.route('/notfound')
def notfound():
    return generate_response("404")
