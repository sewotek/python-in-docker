# Python simple web app

Python is nice.

We are using Python quite often - you should know how to make images for Python applications.

## Your task

Build image serving simple Python application written in Flask framework.

After executing

    docker run --rm -ti -P -e VERSION=0.1 yourdockerid/image-name:tag

target web page should display something like this:

---

Hello from Docker!

---

## Requirements

- you can't modify Python code,
- at /devops web path container should show your email,
- at /version web path container should show provided version,

## Remarks
- want impress us more? let container log speak in your name - what is going on, errors, requests, etc.

